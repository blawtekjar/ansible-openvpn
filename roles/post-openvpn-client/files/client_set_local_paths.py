#!/usr/bin/python

import os, re
from subprocess import check_output

class Color:
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

c = Color()
hostname = check_output("hostname")
vars = {"server-ip":INSERTIPHERE,"server-hostname":INSERTHOSTNAMEHERE}

def make_backup():
	if not os.path.isfile("/etc/hosts.bak"):
		os.system("cp /etc/hosts /etc/hosts.bak")

def get_hosts_file(path):
	serverline = []
	clientlines = []
	serverpush = False
	clientpush = False
	with open(path) as f:
		for line in f:
			if not hostname in line:
				if serverpush and not re.search("#### ANSIBLE OPENVPN CLIENTS",line):
					entry = line.replace('\n', '')
					entry = entry.split("\t")
					appended = {"name":entry[0],"ip":entry[1]}
			 		serverline.append(appended)
				if clientpush and not re.search("#### ANSIBLE OPENVPN END",line):
					entry = line.replace('\n', '')
					entry = entry.split("\t")
					appended = {"name":entry[1],"ip":entry[0]}
				 	clientlines.append(appended)
				if re.search("#### ANSIBLE OPENVPN SERVER",line):
					serverpush = True
				if re.search("#### ANSIBLE OPENVPN CLIENTS",line):
					serverpush = False
					clientpush = True
				if re.search("#### ANSIBLE OPENVPN END",line):
					break
	return clientlines

def set_hosts_file(list_of_hosts,redundant=False):
	inputfile = open('/etc/hosts', 'r').readlines()
	write_file = open('/etc/hosts','w')
	if not redundant:
		for line in inputfile:
		    write_file.write(line)
		    if "#### ANSIBLE OPENVPN CLIENTS" in line:
		       for item in list_of_hosts:
		            new_line = item["ip"]+"\t"+item["name"]
		            write_file.write(new_line + "\n") 
	else:
		for line in inputfile:
			for item in list_of_hosts:
				if re.search(item["name"],line):
					print(c.FAIL+"REMOVING: "+line+c.ENDC)
				else:
					write_file.write(line)
					print(line)
	write_file.close()

def configure_template_hosts():
	inputfile = open('/etc/hosts', 'r').readlines()
	has_template = False
	for line in inputfile:
		if "#### ANSIBLE OPENVPN SERVER" in line:
			has_template = True
	if not has_template:
		template = '''\n\n#### ANSIBLE OPENVPN SERVER
{server-ip}	{server-hostname}
#### ANSIBLE OPENVPN CLIENTS
#### ANSIBLE OPENVPN END'''.format(**vars)
		append_file = open('/etc/hosts','a')
		append_file.write(template)
		append_file.close()
		print(c.WARNING+"Added 'Ansible host template' to /etc/hosts"+c.ENDC)

def update_hosts():
	hosts_clients = get_hosts_file("/etc/hosts")
	ansible_clients = get_hosts_file("hosts")
	missing_hosts = []
	for host in ansible_clients:
		if host not in hosts_clients:
			print(host["name"]+" missing!")
			missing_hosts.append(host)
	print("\nMISSING HOSTS:")
	print(c.WARNING+str(missing_hosts)+c.ENDC)
	if missing_hosts:
		for host in missing_hosts:
			set_hosts_file([host])
	redundant_hosts = []
	for host in hosts_clients:
		if host not in ansible_clients:
			print(host["name"]+" redundant!")
			redundant_hosts.append(host)
	print("\nREDUNTANT HOSTS:")
	print(c.WARNING+str(redundant_hosts)+c.ENDC)
	if redundant_hosts:
		for host in redundant_hosts:
			set_hosts_file([host],redundant=True)
	if not redundant_hosts and not missing_hosts:
		print(c.OKGREEN+"\n### OK! ###\n\n")

make_backup()
configure_template_hosts()
update_hosts()
