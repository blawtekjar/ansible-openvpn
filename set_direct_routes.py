#!/usr/bin/python

import re

def get_ansible_hosts():
	serverline = []
	clientlines = []
	serverpush = False
	clientpush = False
	with open("hosts") as f:
		for line in f:
			if serverpush and not re.search("#### ANSIBLE REMOVE OPENVPN CLIENTS",line):
				entry = line.replace('\n', '')
				entry = entry.split("\t")
				appended = {"name":entry[0],"ip":entry[1]}
			 	serverline.append(appended)
			if clientpush and not re.search("#### ANSIBLE REMOVE OPENVPN END",line):
				entry = line.replace('\n', '')
				entry = entry.replace('ansible_direct=', '')
				entry = entry.split("\t")
				appended = {"name":entry[0],"ip":entry[1]}
			 	clientlines.append(appended)
			if re.search("#### ANSIBLE REMOVE OPENVPN SERVER",line):
				serverpush = True
			if re.search("#### ANSIBLE REMOVE OPENVPN CLIENTS",line):
				serverpush = False
				clientpush = True
			if re.search("#### ANSIBLE REMOVE OPENVPN END",line):
				break
	print clientlines
	return clientlines

def set_hosts_file(list_of_hosts):
	for host in list_of_hosts:
		inputfile = open('/etc/hosts', 'r').readlines()
		write_file = open('/etc/hosts','w')
		for line in inputfile:
			if host["name"] in line:
				print("CHANGING: "+line)
				new_line = host["ip"]+"\t"+host["name"]
				write_file.write(new_line + "\n")
			else:
				write_file.write(line)
		write_file.close()


set_hosts_file(get_ansible_hosts())


