#!/usr/bin/python

import re, os

def make_backup():
	if not os.path.isfile("/etc/hosts.bak"):
		os.system("cp /etc/hosts /etc/hosts.bak")

def get_hosts_file():
	serverline = []
	clientlines = []
	serverpush = False
	clientpush = False
	with open("/etc/hosts") as f:
		for line in f:
			if serverpush and not re.search("#### ANSIBLE OPENVPN CLIENTS",line):
				entry = line.replace('\n', '')
				entry = entry.split("\t")
				appended = {"name":entry[0],"ip":entry[1]}
			 	serverline.append(appended)
			if clientpush and not re.search("#### ANSIBLE OPENVPN END",line):
				entry = line.replace('\n', '')
				entry = entry.split("\t")
				appended = {"name":entry[1],"ip":entry[0]}
			 	clientlines.append(appended)
			if re.search("#### ANSIBLE OPENVPN SERVER",line):
				serverpush = True
			if re.search("#### ANSIBLE OPENVPN CLIENTS",line):
				serverpush = False
				clientpush = True
			if re.search("#### ANSIBLE OPENVPN END",line):
				break
	return clientlines

def set_vpn_ip():
	hosts_clients = get_hosts_file()
	vpn_hosts = []
	# Edit location so it corresponds to keys folder within client-keys
	location = ""
	for path, dirs, files in os.walk(location, topdown=False):
		for name in dirs:
			ip = open(location+"/"+name+"/vpn_ip.txt", 'r').read().replace('\n', '')
			vpn_hosts.append({"name":name,"ip":ip})

	for host in vpn_hosts:
		inputfile = open('/etc/hosts', 'r').readlines()
		write_file = open('/etc/hosts','w')
		for line in inputfile:
		    if host["name"] in line and host["ip"]:
		            new_line = host["ip"]+"\t"+host["name"]
		            write_file.write(new_line + "\n")
		    else:
		    	write_file.write(line)
		write_file.close()

def copy_hosts_file(topath):
	os.system("cp -r /etc/hosts "+topath)

set_vpn_ip()
copy_hosts_file("/home/ansible-master/ansible-openvpn/roles/openvpn-client/files")
